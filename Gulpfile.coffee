# packages
{ src, dest, parallel, watch, series } = require 'gulp'
babel        = require 'gulp-babel'
path         = require 'path'
sourcemaps   = require 'gulp-sourcemaps'
fs           = require 'fs-extra'

# environment variables
NODE_ENV = process.env.NODE_ENV || 'development'

{ dependencies, devDependencies } = config = require path.resolve __dirname, 'package.json'

PREFIX = './dist'

###
# build
###

# prepare
prepare = ->
  fs.ensureDir PREFIX

# js
js = ->

  task = src [ './src/**/*.js' ]
    .pipe sourcemaps.init()
    .pipe babel()
    .pipe sourcemaps.write('.')
    .pipe dest PREFIX

exports.js = js

# build
build = js

exports.build = build

# watch
dev = ->

  watch [ './src/*.js', './src/**/*.js' ], js

exports.dev = dev
exports.default = build

