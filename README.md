# @ipui/clipboard

persistant clipboard for ipui

**this is not an implementation of an entire ipfs node**, this project only use the `api` that the _daemon_ binds in the port `5001` (by default). [go](https://github.com/ipfs/go-ipfs) or [javascript](https://github.com/ipfs/js-ipfs) implementation is by your own.

> This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## ip[fn]s links

## description

this provider uses dag api of ipfs to store a kind of clipboard accesible from any piece of your application.

## technologies

* [reactjs](https://reactjs.org)
* [ipfs](https://ipfs.io)

## depends on

* [@ipui/ipfs](https://gitlab.com/ipui/ipui-ipfs)

## how it works

* it uses the [context api](https://reactjs.org/docs/context.html) of reactjs to provide an accessible service to any component inside the context itself.

* uses [mutable file system](https://github.com/ipfs/interface-js-ipfs-core/blob/master/SPEC/FILES.md#mutable-file-system) of ipfs to store a config file that contains the [dag](https://github.com/ipfs/interface-js-ipfs-core/blob/master/SPEC/DAG.md) address of the clipboard (maybe there are a better ways to do this task, would you like to share?).

* uses [dag](https://github.com/ipfs/interface-js-ipfs-core/blob/master/SPEC/DAG.md) to store the elements of the clipboard.

* binds `withClipboard` on the `props` or `this.props` when is used as _consumer_.

## propTypes

| name | default | description |
| - | - | - |
| `config` | | where the config file is stored or needs to be. |

## state

this state variables are accesible by the component that uses `withClipboard` consumer.

### clip

contains the actual elements in the clipboard.

## methods

this methods are accesible by the component that uses `withClipboard` consumer.

### put

> put one element in the clipboard.

**put( item )**

* `item` any type of value that can be stored in a dag node.

### remove

> remove an element `index` of the collection.

**remove( index )**

* `index` is a position in the collection of the element that needs to be removed.

### clean

> reset the clipboard state to an empty array `[]`.

**clean()**

## usage

### provider

```javascript
import Clipboard from '@ipui/clipboard'

const MyComponent = props => {

  return (
    <Clipboard filepath="/.conf/my-app/clipboard">
      
      { /*
        your app code that requires a clipboard provider
      */}

    </Clipboard>
  )
}

export default MyComponent
```

### consumer

put file example

```javascript
import { withClipboard } from '@ipui/clipboard'

const PutFile = props => {

  function handleClick() {
    const { clip, put } = props.withClipboard
    const { file } = props
    if ( !inClip( file ) )
      put( file )
  }

  function inClip( file ) {
    const { clip } = props.withClipboard
    return clip.filter( el => el.hash === file.hash ).shift() || false
  }

  return (
    <button onClick={ handleClick }>
      copiar
    </button>
  )
}

PutFile.propTypes = {
  file: PropTypes.object.isRequired
}

export default withClipboard( PutFile )
```

remove element example

```javascript
import { withClipboard } from '@ipui/clipboard'

const RemoveProduct = props => {

  function handleClick() {
    const { remove } = props.withClipboard
    const { product } = props

    const index = cartIndex( product )

    if ( index >= 0 )
      remove( index )
  }

  cartIndex( product ) {
    const { clip } = props.withClipboard
    const item = clip.filter( el => el.sku === product.sku ).shift()
    if ( !item )
      return -1

    return clip.indexOf( item )
  }

  return (
    <button onClick={ handleClick }>
      remove from cart
    </button>
  )
}

RemoveProduct.propTypes = {
  product: PropTypes.object.isRequired
}

export default withClipboard( RemoveProduct )
```

## last updated
aug 24, 2019


