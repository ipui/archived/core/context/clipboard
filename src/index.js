import React, { Component, createContext } from 'react'
import PropTypes from 'prop-types'
import { withIpfs } from '@ipui/ipfs'

const ClipboardContext = createContext()

class Clipboard extends Component {

  static propTypes = {
    config: PropTypes.string.isRequired
  }

  state = {
    clip: []
  }

  constructor( props ) {
    super( props )
    this.bootstrap = this.bootstrap.bind( this )

    // collection
    this.put = this.put.bind( this )
    this.remove = this.remove.bind( this )
    this.clean = this.clean.bind( this )

    // file
    this.loadCidFromFile = this.loadCidFromFile.bind( this )
    this.saveCidToFile = this.saveCidToFile.bind( this )

    // dag
    this.initClip = this.initClip.bind( this )
    this.loadClip = this.loadClip.bind( this )
    this.saveClip = this.saveClip.bind( this )
  }

  componentDidMount() {
    this.bootstrap()
  }

  bootstrap() {
    this.loadCidFromFile()
      .then( cid => {
        return this.loadClip( cid )
      } )
      .then( clip => {
        this.setState( {
          ...this.state,
          clip: clip.value
        } )
      } )
      .catch( e => {
        return this.initClip()
      } )
      .catch( e => {
        console.error( 'after', e )
      } )
  }

  put( item ) {
    const { clip } = this.state
    clip.push( item )
    this.saveClip( clip )
  }

  remove( index ) {
    const { clip } = this.state
    clip.splice( index, 1 )
    this.saveClip( clip )
  }

  clean() {
    this.saveClip( [] )
  }

  loadCidFromFile() {
    const { read } = this.props.withIpfs.node.files
    const { config } = this.props
    return read( config )
  }

  saveCidToFile( newCid ) {
    const { write } = this.props.withIpfs.node.files
    const { config } = this.props
    return write( config, Buffer.from( newCid.toBaseEncodedString() ), {
      create: true,
      parents: true
    } )
  }

  initClip() {
    const { put } = this.props.withIpfs.node.dag
    return new Promise( ( resolve, reject ) => {
      return put( [] )
        .then( cid => {
          resolve( this.saveCidToFile( cid ) )
        } )
        .catch( reject )
    } )
  }

  loadClip( cid ) {
    const { get } = this.props.withIpfs.node.dag
    return get( cid )
  }

  saveClip( newClip ) {
    const { put } = this.props.withIpfs.node.dag
    put( newClip )
      .then( cid => {
        this.setState( {
          ...this.state,
          clip: newClip
        } )
        return this.saveCidToFile( cid )
      } )
      .catch( err => {
        console.error( 'save clip', err )
      } )
  }

  render() {
    const { children } = this.props
    const { clip } = this.state
    const {
      put,
      remove,
      clean
    } = this

    return (
      <ClipboardContext.Provider value={ {
        clip,
        put,
        remove,
        clean
      } }>
        { children }
      </ClipboardContext.Provider>
    )
  }
}

const withClipboard = ( ComponentAlias ) => {

  return props => (
    <ClipboardContext.Consumer>
      { context => {
        return <ComponentAlias { ...props } withClipboard={ context } />
      } }
    </ClipboardContext.Consumer>
  )

}

export default withIpfs( Clipboard )

export {
  ClipboardContext,
  withClipboard
}
